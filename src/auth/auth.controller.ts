import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { ResponseService } from '../response/response.service';
import { Response, ResponseStatusCode } from '../response/response.decorator';
import { IResponse } from '../response/response.interface';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';

@Controller('/auth')
export class AuthController {
  constructor(
    @Response() private readonly responseService: ResponseService,
    private readonly authService: AuthService,
  ) {}

  @ResponseStatusCode()
  @UseGuards(AuthGuard('local'))
  @Post('/login')
  async login(@Request() req): Promise<IResponse> {
    const result = this.authService.login(req.user);
    return this.responseService.success('OK',req.user);
  }
}
